option(
	'examples',
	type: 'array',
	choices: [
		'simple',
		'opengl',
		'',
	],
	value: [
		'simple',
	],
)

option(
	'drivers',
	type: 'array',
	choices: [
		'rift',
		'deepoon',
		#'psvr', disabled for 0.3
		'vive',
		'nolo',
		'wmr',
		'xgvr',
		'external',
		'android',
	],
	value: [
		'rift',
		'deepoon',
		#'psvr', disabled for 0.3
		'vive',
		'nolo',
		'wmr',
		'xgvr',
		'external',
	],
)

option(
	'hidapi',
	type: 'combo',
	choices: [
		'auto',
		'libusb',
		'hidraw'
	],
	value: 'auto',
)
